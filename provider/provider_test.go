package provider

import (
	"fmt"
	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
	"github.com/pact-foundation/pact-go/utils"
	"log"
	"net/http"
	"os"
	"testing"
)

func TestPactProvider(t *testing.T) {

	if os.Getenv("CI") != "" {
		t.Skip("Skipping testing in CI environment")
	}
	pact := createPact()

	go startServer()

	// Authorization middleware
	// This is your chance to modify the request before it hits your provider
	// NOTE: this should be used very carefully, as it has the potential to
	// _change_ the contract
	f := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			r.Header.Add("Authorization", "Bearer 1234-dynamic-value")
			next.ServeHTTP(w, r)
		})
	}

	_, err := pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL:            "http://localhost:8000",
		PactURLs:                   []string{"https://seremet.pactflow.io/pacts/provider/provider/consumer/consumer/latest/test"},
		BrokerToken:                "e3jhCwyIQEV5slPOOuj3Jg",
		PublishVerificationResults: true,
		ProviderVersion:            "1.0.0",
		RequestFilter:              f,
		StateHandlers: types.StateHandlers{
			"hello world todo object": func() error {
				name = "billy"
				return nil
			},
		},
	})

	if err != nil {
		t.Log(err)
	} else {
		log.Printf("API terminating:")
	}
}

var name = "some other name"

func startServer() {
	hh := GetHTTPHandler()
	log.Fatal(http.ListenAndServe(":8001", hh))
}

var dir, _ = os.Getwd()
var logDir = fmt.Sprintf("%s/log", dir)
var port, _ = utils.GetFreePort()

// Setup the Pact client.
func createPact() dsl.Pact {
	return dsl.Pact{
		Provider: "provider",
		Consumer: "consumer",
		LogDir:   logDir,
		LogLevel: "INFO",
	}
}
