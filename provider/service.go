package provider

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"

	//"strconv"

	"gitlab.com/okanseremet/todo-golang/model"
	"gitlab.com/okanseremet/todo-golang/provider/repository"
)

type Return struct {
	Message string      `json:"message"`
	Todos   interface{} `json:"todos"`
}

var todoRepo = &repository.TodoRepository{
	CurrentID: 0,
	Todos:     model.Todos{},
}

func GetTodos(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	res := Response{w}
	res.SendJSON(todoRepo.GetTodos())
}

func AddTodo(w http.ResponseWriter, r *http.Request) {
	var todo model.Todo
	res := Response{w}
	_return := &Return{}
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &todo); err != nil {
		panic(err)
	}

	todoRes := todoRepo.CreateTodo(todo)

	_return.Message = "todo created and added."
	_return.Todos = todoRes
	res.SendJSON(_return)
}

func DeleteTodo(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := 1
	var err error
	_return := &Return{}
	value, ok := vars["id"]
	fmt.Println("value:", value, "is exists:", ok)
	res := Response{w}
	if id, err = strconv.Atoi(value); err != nil {
		panic(err)
	}
	todo := todoRepo.DeleteTodo(id)
	if todo == nil {
		_return.Message = "todo was deleted"
		_return.Todos = todo
		res.SendJSON(_return)
		return
	}

}

func MarkTodo(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := 1
	var err error
	_return := &Return{}
	value, ok := vars["id"]

	fmt.Println("value:", value, "is exists:", ok)
	res := Response{w}
	if id, err = strconv.Atoi(value); err != nil {
		panic(err)
	}
	todo, err := todoRepo.MarkTodo(id)
	if err == nil {
		_return.Message = "todo checked successfully."
		_return.Todos = todo
		res.SendJSON(_return)
		return
	}

}

func navHome(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the API!")
	fmt.Println("Hit Api")
}

func GetHTTPHandler() *mux.Router {

	r := mux.NewRouter()

	cors := handlers.CORS(
		handlers.AllowedHeaders([]string{"content-type"}),
		handlers.AllowedOrigins([]string{"*"}),
		handlers.AllowCredentials(),
	)

	r.Use(cors)

	// Define a subrouter
	api := r.PathPrefix("/api").Subrouter()

	api.
		Methods("GET", "OPTIONS").
		Path("").
		HandlerFunc(navHome)

	api.
		Methods("GET", "OPTIONS").
		Path("/todos").
		HandlerFunc(GetTodos)

	api.
		Methods("POST", "OPTIONS").
		Path("/todos").
		HandlerFunc(AddTodo)

	api.
		Methods("DELETE", "OPTIONS").
		Path("/todos/{id}").
		HandlerFunc(DeleteTodo)

	api.
		Methods("PUT", "OPTIONS").
		Path("/todos/{id}").
		HandlerFunc(MarkTodo)

	return api
}
