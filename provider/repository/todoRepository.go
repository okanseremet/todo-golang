package repository

import (
	"fmt"
	. "gitlab.com/okanseremet/todo-golang/model"
)

type TodoRepository struct {
	Todos     Todos
	CurrentID int
}

func (tr *TodoRepository) GetTodos() Todos {
	response := Todos{}

	for _, todo := range tr.Todos {
		response = append(response, todo)
	}

	return response
}

func (tr *TodoRepository) CreateTodo(t Todo) Todo {
	tr.CurrentID++
	t.Id = tr.CurrentID
	tr.Todos = append(tr.Todos, t)
	return t
}

func (tr *TodoRepository) MarkTodo(id int) (Todo, error) {

	for i, t := range tr.Todos {
		if t.Id == id {
			tr.Todos[i].IsChecked = !tr.Todos[i].IsChecked
			//tr.Todos = append(tr.Todos, t)
			return tr.Todos[i], nil
		}
	}
	return Todo{}, fmt.Errorf("could not find Todo with id of %d to mark", id)
}

func (tr *TodoRepository) DeleteTodo(id int) error {
	for i, t := range tr.Todos {
		if t.Id == id {
			tr.Todos = append(tr.Todos[:i], tr.Todos[i+1:]...)
			return nil
		}
	}

	return fmt.Errorf("could not find Todo with id of %d to delete", id)
}
