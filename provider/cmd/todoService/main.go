package main

import (
	"github.com/gorilla/handlers"
	"log"
	"net/http"

	"gitlab.com/okanseremet/todo-golang/provider"
)

func main() {
	hh := provider.GetHTTPHandler()

	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})
	log.Printf("API starting: port %d", 8080)
	log.Fatal(http.ListenAndServe(":8080", handlers.CORS(originsOk, headersOk, methodsOk)(hh)))
	//log.Printf("API terminating: %v", http.Serve(ln, hh))
}
