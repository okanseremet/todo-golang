package provider

import (
	"encoding/json"
	"net/http"
)

type Response struct {
	writer http.ResponseWriter
}

func (r Response) SendJSON(data interface{}) {
	r.writer.Header().Set("Content-Type", "application/json; charset=utf-8")
	r.writer.Header().Set("Access-Control-Allow-Origin", "*")
	r.writer.WriteHeader(http.StatusOK)

	resBody, _ := json.Marshal(data)

	if _, err := r.writer.Write(resBody); err != nil {
		panic(err)
	}
}
