package model

type Todo struct {
	Text      string `json:"text"`
	IsChecked bool   `json:"isChecked"`
	Id        int    `json:"id"`
}

type Todos []Todo

type AddTodoRequest struct {
}

type AddTodoResponse struct {
}
