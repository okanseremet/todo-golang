FROM golang:1.15-alpine as build

RUN mkdir -p /go/src/gitlab.com/okanseremet/todo-golang
WORKDIR /go/src/gitlab.com/okanseremet/todo-golang

# Copy the Go App to the container
COPY . .

#RUN go mod init

RUN go build -o todo-app /go/src/gitlab.com/okanseremet/todo-golang/provider/cmd/todoService/main.go

FROM alpine

COPY --from=build go/src/gitlab.com/okanseremet/todo-golang/todo-app /todo-app

EXPOSE 8080

CMD ["./todo-app"]

