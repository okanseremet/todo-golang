TEST?=./...

include ./make/config.mk

install:
	@if [ ! -d pact/bin ]; then\
		echo "--- 🛠 Installing Pact CLI dependencies";\
		curl -fsSL https://raw.githubusercontent.com/pact-foundation/pact-ruby-standalone/master/install.sh | bash;\
    fi


run-provider:
	@go run provider/cmd/usersvc/main.go

provider: install
	@echo "--- 🔨Running Provider Pact tests "
	go test -count=1 -tags=integration gitlab.com/okanseremet/todo-golang/provider

.PHONY: install unit provider run-provider

